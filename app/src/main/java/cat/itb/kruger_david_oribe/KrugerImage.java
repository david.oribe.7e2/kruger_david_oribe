package cat.itb.kruger_david_oribe;

import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

public class KrugerImage {
    private Uri mImageUrl;
    public KrugerImage() {
        //empty constructor needed
    }

    public KrugerImage(Uri imageUrl) {


        mImageUrl = imageUrl;
    }


    public Uri getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        mImageUrl = imageUrl;
    }
}
