package cat.itb.kruger_david_oribe;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class StartFragment extends Fragment {

    @BindView(R.id.btnGoToMap)
    Button btnGoToMap;

    private StartViewModel mViewModel;
/*
    private static final String CARPETA_PRINCIPAL ="misImagenesApp/"; //CARPETA_PRINCIPAL
    private static final String CARPETA_IMAGEN ="KrugerPhotos";//CARPETA_IMAGEN
    private static final String DIRECTORIO_IMAGEN = CARPETA_PRINCIPAL + CARPETA_IMAGEN;//DIRECTORIO_IMAGEN
    private String path;
    File imageFile;
    Bitmap bitmap;

 */

    private MapsFragment mapsFragment;
    public static StartFragment newInstance() {
        return new StartFragment();
    }
    private MediaPlayer mediaPlayer;
    OnHeadlineSelectedListener callback;

    public void setOnHeadlineSelectedListener(OnHeadlineSelectedListener callback) {
        this.callback = callback;
    }

    // This interface can be implemented by the Activity, parent Fragment,
    // or a separate test implementation.
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(int position);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //mViewModel = ViewModelProviders.of(this).get(StartViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        mapsFragment=new MapsFragment();
        //myStorage = FirebaseStorage.getInstance().getReference();



    }

    private void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.release();
        mediaPlayer=null;

    }

    @OnClick({R.id.btnGoToMap})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGoToMap:
                mediaPlayer=MediaPlayer.create(getContext(), R.raw.leopardosound);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(this::onCompletion);
                Intent intent= new Intent(getContext(),MapsFragment.class);
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(intent);
                }

                break;


        }
    }


}
