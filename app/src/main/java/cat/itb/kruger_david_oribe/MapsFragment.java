package cat.itb.kruger_david_oribe;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsFragment extends FragmentActivity implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,GoogleMap.OnInfoWindowLongClickListener,
        OnMapReadyCallback {

    @BindView(R.id.makePhotoBtn)
    Button makePhotoBtn;
    @BindView(R.id.markerImage)
    ImageView markerImage;
    @BindView(R.id.hideBtn)
    Button hideBtn;

    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationManager locationManager;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean isAllowed;

    private static final int COD_PHOTO = 20;

    private StorageReference myStorage;
    private Uri photoURI;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maps_fragment);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        myStorage = FirebaseStorage.getInstance().getReference();
        markerImage.setVisibility(View.INVISIBLE);


    }

    private View mContentView;



    private void moveCamera() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            LatLng currentPos = new LatLng(location.getLatitude(), location.getLongitude());
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(currentPos)
                                    .zoom(10)
                                    .tilt(30)
                                    .build();
                            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            /*
                            mMap.addMarker(new MarkerOptions().position(currentPos).title("You are Here!")
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                    .snippet(""));

                             */
                        }
                    }
                });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        getLocationPermission();
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        getImagesFireBase();
        mMap.setOnInfoWindowLongClickListener(this);


        LatLng kruger = new LatLng(-23.988323, 31.553894);
        LatLng olifantsCamp = new LatLng(-24.006038, 31.740272);
        LatLng letabaRestCamp = new LatLng(-23.854223, 31.574431);
        LatLng bergDalCamp = new LatLng(-25.420010, 31.451220);
        LatLng crocoBridgeCamp = new LatLng(-25.358910, 31.892040);
        LatLng lowerSabie = new LatLng(-25.119830, 31.915940);
        LatLng orpenCamp = new LatLng(-24.452870, 31.405420);
        LatLng pretoriusCamp = new LatLng(-25.169230, 31.268720);
        LatLng pundaCamp = new LatLng(-22.691900, 31.018130);
        LatLng sataraCamp = new LatLng(-24.392580, 31.779530);
        LatLng shingwedziCamp = new LatLng(-23.113570, 31.433140);
        LatLng skukuzaCamp = new LatLng(-24.989840, 31.592630);






        mMap.addMarker(new MarkerOptions().position(kruger).title("KRUGER NATIONAL PARK!")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .snippet(""));



        mMap.addMarker(new MarkerOptions().position(olifantsCamp).title("Olifants Camp")
                .snippet("The panoramic view from Olifants Rest Camp provides a perfect vantage point from which to spot wildlife."));
        mMap.addMarker(new MarkerOptions().position(letabaRestCamp).title("Letaba Rest Camp")
                .snippet("A green and thriving oasis, Letaba Camp offers comfortable " +
                        "accommodation in Kruger Park with excellent Elephant and bird viewing opportunities."));
        mMap.addMarker(new MarkerOptions().position(bergDalCamp).title("Berg-en-Dal Rest Camp")
                .snippet("Berg en Dal Camp is situated on the banks of the Matjulu spruit, " +
                        "in the south western corner of the Kruger National Park. The camp is only a 5 hours drive from Johannesburg."));
        mMap.addMarker(new MarkerOptions().position(crocoBridgeCamp).title("Crocodile Bridge Rest Camp")
                .snippet("Just a stone's throw from the Mozambican border and Maputo, Crocodile Bridge Rest Camp in Kruger National Park is rich in local history."));
        mMap.addMarker(new MarkerOptions().position(lowerSabie).title("Lower Sabie Rest Camp")
                .snippet("Lower Sabie Rest Camp in Kruger Park is on the banks of the perennial Sabie River, which draws a wide variety of animals all year round."));
        mMap.addMarker(new MarkerOptions().position(orpenCamp).title("Orpen Rest Camp")
                .snippet("Orpen Rest Camp is set amongst rock gardens vibrant with Aloes and Barberton Daisies. This small camp is situated at the Orpen Gate entrance to the Kruger National Park."));
        mMap.addMarker(new MarkerOptions().position(pretoriusCamp).title("Pretoriuskop Rest Camp")
                .snippet("Pretoriuskop Camp is the oldest Rest Camp in the Kruger Park and holds a large population of White Rhino."));
        mMap.addMarker(new MarkerOptions().position(pundaCamp).title("Punda Maria Rest Camp")
                .snippet("Punda Maria Rest Camp has lush vegetation which attracts a wide variety of rare bird species and wildlife."));
        mMap.addMarker(new MarkerOptions().position(sataraCamp).title("Satara Rest Camp")
                .snippet("The ambience of Satara Camp recalls the mood of yester-year Africa with red-roofed public buildings, thatched bungalows and neatly raked paths."));
        mMap.addMarker(new MarkerOptions().position(shingwedziCamp).title("Shingwedzi Rest Camp")
                .snippet("Shingwedzi Rest Camp in Kruger National Park is situated in prime elephant and buffalo country where large herds can be found.."));
        mMap.addMarker(new MarkerOptions().position(skukuzaCamp).title("Skukuza Rest Camp")
                .snippet("Skukuza Camp is the capital of Kruger National Park. It is the biggest camp and includes facilities such as a shop, ATM, internet cafe, restaurant, library, fuel station and more."));

        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(kruger, 10));
        //if (isAllowed) {
        moveCamera();
        //}
        // Add a marker in Sydney and move the camera

        //mMap.setOnMapLongClickListener(this);
        //mMap.setOnInfoWindowLongClickListener(this);
        //GoogleMap.setOnMarkerClickListener()
    }

    private void getLocationPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {

            mMap.setMyLocationEnabled(true);
            isAllowed = true;
        } else {
            Toast.makeText(this, "Denied Location Permission", Toast.LENGTH_SHORT).show();
            //isAllowed=false;
            // Permission was denied. Display an error message.
        }
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {


            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,
                        "cat.itb.Kruger_David_Oribe",//nombre authorities del provider en el AndroidManifest.xml
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                startActivityForResult(takePictureIntent, COD_PHOTO);
            }
        }
    }

    private String currentPhotoPath;

    private File createImageFile() throws IOException { //Guardar foto
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    LatLng currentPos;
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COD_PHOTO && resultCode == RESULT_OK) { //Si eel intent que está haciendo es el de hacer foto entrará en el if
            Uri file = photoURI;
            StorageReference storageRef = myStorage.child("KrugerImages/" + file.getLastPathSegment());

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                currentPos = new LatLng(location.getLatitude(), location.getLongitude());
                                StorageMetadata metadataImage = new StorageMetadata.Builder()
                                        .setCustomMetadata("lat", String.valueOf(currentPos.latitude))
                                        .setCustomMetadata("long", String.valueOf(currentPos.longitude))
                                        .build();
                                UploadTask uploadTask = storageRef.putFile(file,metadataImage);

                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        //mapsFragment.addPhotoMarker();
                                        getImagesFireBase();
                                        //addPhotoMarker(); //Añade un marcador verde con las fotos subidas
                                        Toast.makeText(getApplicationContext(), "Photo Uploaded to FireBase!", Toast.LENGTH_SHORT).show();

                                    }
                                });
                            }
                        }
                    });



        }
    }

    private void getImagesFireBase() {
        myStorage.child("KrugerImages").listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {

                        for (StorageReference item : listResult.getItems()) {
                            //Uri photoUri=item.getDownloadUrl().getResult();

                            item.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                                @Override
                                public void onSuccess(StorageMetadata storageMetadata) {
                                    LatLng currentPos = new LatLng(Double.valueOf(storageMetadata.getCustomMetadata("lat")), Double.valueOf(storageMetadata.getCustomMetadata("long"))); //COGE LA LATITUD Y LONGITUD DE LA FOTO

                                    Marker marker=mMap.addMarker(new MarkerOptions().position(currentPos).title("Uploaded photo. Lat: "+currentPos.latitude+"\nLong: "+currentPos.longitude)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                            .snippet(""));
                                    getImageUrlFromFirebase(item,marker);

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {

                                }
                            });
                            //getImageUrlFromFirebase(item, krugerImage); //TODO VOY X AQUÍ
                            //imagesList.add(krugerImage);
                            //imagesList.add(item);
                            // All the items under listRef.
                        }

                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Uh-oh, an error occurred!
                    }
                });
    }

    private void getImageUrlFromFirebase(StorageReference ref, Marker marker){
        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                //Toast.makeText(getApplicationContext(), uri.toString(), Toast.LENGTH_SHORT).show();
                marker.setSnippet(uri.toString());
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.i("IMG_NOT_FOUND","The list of pictures is not available");
                exception.printStackTrace();
            }
        });
    }



    @OnClick({R.id.makePhotoBtn, R.id.hideBtn})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.makePhotoBtn:
                dispatchTakePictureIntent();
                break;
            case R.id.hideBtn:
                markerImage.setVisibility(View.INVISIBLE);
                break;


        }
    }



    @Override
    public void onInfoWindowLongClick(Marker marker) {
        if(marker.getTitle().startsWith("Upl")) {
            Toast.makeText(this, "Long Click on Marker",
                    Toast.LENGTH_SHORT).show();
            myStorage.child("KrugerImages").listAll()
                    .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                        @Override
                        public void onSuccess(ListResult listResult) {

                            for (StorageReference item : listResult.getItems()) {
                                //Uri photoUri=item.getDownloadUrl().getResult();

                                getImageUrlFromFirebase(item, marker);
                                item.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                                    @Override
                                    public void onSuccess(StorageMetadata storageMetadata) {
                                        //COMPARAR METADATOS

                                        Glide.with(getApplicationContext()) //Como la accedo a la imagen de manera externa para guardarla desde la app ya no podré utilizar Bitmap. Glide permite escalar la imagen a la hora de mostrarla.
                                                .load(marker.getSnippet())
                                                .transform(new CircleCrop()) //mete la foto en un círculo. Hay más funciones como FitCenter(), CenterCrop() o hasta crear tu propia transformación:; .transform(new MultiTransformation(new FitCenter(), new YourCustomTransformation())
                                                .into(markerImage);

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {

                                    }
                                });
                                //getImageUrlFromFirebase(item, krugerImage); //TODO VOY X AQUÍ
                                //imagesList.add(krugerImage);
                                //imagesList.add(item);
                                // All the items under listRef.
                            }

                        }

                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Uh-oh, an error occurred!
                        }
                    });
            markerImage.setVisibility(View.VISIBLE);
            //getImageUrlFromFirebase(myStorage);
        /*
        Glide.with(getApplicationContext()) //Como la accedo a la imagen de manera externa para guardarla desde la app ya no podré utilizar Bitmap. Glide permite escalar la imagen a la hora de mostrarla.
                .load()
                .transform(new CircleCrop()) //mete la foto en un círculo. Hay más funciones como FitCenter(), CenterCrop() o hasta crear tu propia transformación:; .transform(new MultiTransformation(new FitCenter(), new YourCustomTransformation())
                .into(markerImage);

         */
        }


    }
}
